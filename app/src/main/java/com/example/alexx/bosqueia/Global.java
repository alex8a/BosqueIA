package com.example.alexx.bosqueia;

/**
 * Created by Alexx on 17/11/2016.
 */

public class Global {
    public static boolean permisoParaEntregar=true;
    public static boolean dlgPersonajeAbierto=false;

    final public static int MOMBO = 0;
    final public static int PIROLO = 1;
    final public static int LUCAS = 2;
    final public static int NUMPERSONAJES = 3;

    final public static int MONTANA = 0;
    final public static int AGUA = 1;
    final public static int BARRANCO =2;
    final public static int PLANO = 3;
    final public static int MURO = 4;
    final public static int CASA = 5;

    public static int xInicialEntrenamiento = 0;
    public static int yInicialEntrenamiento = 0;
    public static int xCasa = 0;
    public static int yCasa = 0;

    final public static int numFilas = 20;
    final public static int numColumnas = 15;

    final public static float EsfuerzoMomboMontana = 2.5f;
    final public static float EsfuerzoMomboAgua = 0.3f;
    final public static float EsfuerzoMomboBarranco = 1.5f;
    final public static float EsfuerzoMomboPlano = 1.0f;

    final public static float EsfuerzoPiroloMontana = 0.3f;
    final public static float EsfuerzoPiroloAgua = 2.5f;
    final public static float EsfuerzoPiroloBarranco = 1.0f;
    final public static float EsfuerzoPiroloPlano = 1.5f;

    final public static float EsfuerzoLucasMontana = 1.5f;
    final public static float EsfuerzoLucasAgua = 1.0f;
    final public static float EsfuerzoLucasBarranco = 2.5f;
    final public static float EsfuerzoLucasPlano = 0.3f;

    public static int[][] modeloMapa;

    public static Nodo[][] nodos;
    public static int numNodosValidos=0;
}
