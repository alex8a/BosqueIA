package com.example.alexx.bosqueia;

/**
 * Created by Alexx on 30/11/2016.
 */

public class Ruta {
    public float esfuerzos[];
    public float ponderaciones[];
    public float pM, pP, pL;
    public Nodo nodoDestino;

    public Ruta(Nodo nodo, float eMombo, float ePirolo, float eLucas){
        esfuerzos=new float[Global.NUMPERSONAJES];
        esfuerzos[Global.MOMBO]=eMombo;
        esfuerzos[Global.PIROLO]=ePirolo;
        esfuerzos[Global.LUCAS]=eLucas;

        ponderaciones=new float[Global.NUMPERSONAJES];
        ponderaciones[Global.MOMBO]=Float.MAX_VALUE/2;
        ponderaciones[Global.PIROLO]=Float.MAX_VALUE/2;
        ponderaciones[Global.LUCAS]=Float.MAX_VALUE/2;

        nodoDestino=nodo;
    }

}
