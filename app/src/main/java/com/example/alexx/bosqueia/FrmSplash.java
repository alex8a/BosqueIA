package com.example.alexx.bosqueia;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

/**
 * Created by Alexx on 17/11/2016.
 */

public class FrmSplash extends Activity implements SeekBar.OnSeekBarChangeListener{
    Button cmdOk;
    SeekBar seekMontana, seekAgua, seekBarranco, seekMuro;
    int porMontana, porAgua, porBarranco, porMuro;
    TextView lblPorMontana, lblPorAgua, lblPorBarranco, lblPorMuro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frmsplash_layout);

        porMontana=10;
        porAgua=5;
        porBarranco=0;
        porMuro=5;

        Global.modeloMapa = new int[20][15];
        for(int i=0;i<20;i++){
            for(int j=0;j<15;j++){
                Global.modeloMapa[i][j]=Global.PLANO;
            }
        }

        lblPorMontana=(TextView)findViewById(R.id.lblPorMontana);
        lblPorAgua=(TextView)findViewById(R.id.lblPorAgua);
        lblPorBarranco=(TextView)findViewById(R.id.lblPorBarranco);
        lblPorMuro=(TextView)findViewById(R.id.lblPorMuro);

        seekMontana=(SeekBar)findViewById(R.id.seekMontana);
        seekMontana.setMax(30);
        seekMontana.setOnSeekBarChangeListener(this);
        seekAgua=(SeekBar)findViewById (R.id.seekAgua);
        seekAgua.setMax(45);
        seekAgua.setOnSeekBarChangeListener(this);
        seekBarranco=(SeekBar)findViewById(R.id.seekBarranco);
        seekBarranco.setMax(30);
        seekBarranco.setOnSeekBarChangeListener(this);
        seekMuro=(SeekBar)findViewById(R.id.seekMuro);
        seekMuro.setMax(45);
        seekMuro.setOnSeekBarChangeListener(this);

        cmdOk=(Button)findViewById(R.id.cmdOk);
        cmdOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(porMontana+porAgua+porBarranco+porMuro<=100){
                    int[] casillas = new int[5];
                    casillas[Global.MONTANA]=(300*porMontana)/100;
                    casillas[Global.AGUA]=(300*porAgua)/100;
                    casillas[Global.BARRANCO]=(300*porBarranco)/100;
                    casillas[Global.MURO]=(300*porMuro)/100;
                    casillas[Global.PLANO]=300-casillas[Global.MONTANA]-casillas[Global.AGUA]-casillas[Global.BARRANCO]-casillas[Global.MURO];
                    int x,y;
                    Random random = new Random();
                    int relieveActual=Global.MONTANA; //0
                    boolean relievesAcabados=false;
                    while(relieveActual<=Global.MURO){
                        while(casillas[relieveActual]<=0){
                            relieveActual++;
                            if(relieveActual>Global.MURO){
                                relievesAcabados=true;
                                break;
                            }
                        }
                        if(relievesAcabados)    break;
                        x=random.nextInt(15);
                        y=random.nextInt(20);
                        if(Global.modeloMapa[y][x]==Global.PLANO){
                            Global.modeloMapa[y][x]=relieveActual;
                            casillas[relieveActual]--;
                        }
                    }
                    do{
                        Global.xCasa=random.nextInt(15);
                        Global.yCasa=random.nextInt(20);
                    }while(Global.modeloMapa[Global.yCasa][Global.xCasa]==Global.MURO);
                    Global.modeloMapa[Global.yCasa][Global.xCasa]=Global.CASA;
                    do{
                        Global.xInicialEntrenamiento=random.nextInt(15);
                        Global.yInicialEntrenamiento=random.nextInt(20);
                    }while(Global.modeloMapa[Global.yInicialEntrenamiento][Global.xInicialEntrenamiento]==Global.MURO&&
                            (Global.yInicialEntrenamiento==Global.yCasa&&Global.xInicialEntrenamiento==Global.xCasa));
                    startActivity(new Intent(FrmSplash.this, FrmMapa.class));
                    finish();
                }else Toast.makeText(FrmSplash.this, "Los porcentajes no deben sobrepasar el 100%", Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()){
            case R.id.seekMontana:
                porMontana=progress+10;
                lblPorMontana.setText(porMontana+"%");
                break;
            case R.id.seekAgua:
                porAgua=progress+5;
                lblPorAgua.setText(porAgua+"%");
                break;
            case R.id.seekBarranco:
                porBarranco=progress;
                lblPorBarranco.setText(porBarranco+"%");
                break;
            case R.id.seekMuro:
                porMuro=progress+5;
                lblPorMuro.setText(porMuro+"%");
                break;
            default:
                break;
        }

    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}
}
