package com.example.alexx.bosqueia;

import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.lang.reflect.Field;

import static android.view.View.GONE;

public class FrmMapa extends AppCompatActivity {
    Grafo grafo;
    RelativeLayout matrizMapa[][];
    LinearLayout ProgressLayout;
    LinearLayout cmdMombo, cmdPirolo, cmdLucas;
    DlgEntrenador dlgEntrenador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frmmapa_layout);
        ProgressLayout=(LinearLayout)findViewById(R.id.ProgressLayout);
        RelativeLayout matrizMapa[][]=new RelativeLayout[20][15];
        dlgEntrenador=new DlgEntrenador();
        getFragmentManager().beginTransaction().add(R.id.fragmentContainer, dlgEntrenador, "dlgEntrenador").commit();
        for(int i=0;i<20;i++){
            for(int j=0;j<15;j++){
                matrizMapa[i][j]=(RelativeLayout)findViewById(getResources().getIdentifier("m"+i+"_"+j, "id", getApplicationContext().getPackageName()));
                switch(Global.modeloMapa[i][j]){
                    case Global.MONTANA:
                        matrizMapa[i][j].setBackgroundResource(R.drawable.montanhaicon);
                        break;
                    case Global.AGUA:
                        matrizMapa[i][j].setBackgroundResource(R.drawable.aguaicon);
                        break;
                    case Global.BARRANCO:
                        matrizMapa[i][j].setBackgroundResource(R.drawable.holeicon);
                        break;
                    case Global.PLANO:
                        matrizMapa[i][j].setBackgroundResource(R.drawable.plainicon);
                        break;
                    case Global.MURO:
                        matrizMapa[i][j].setBackgroundResource(R.drawable.treeicon);
                        break;
                    case Global.CASA:
                        matrizMapa[i][j].setBackgroundResource(R.drawable.houseicon);
                        break;
                    default:
                        break;
                }
            }
        }
        cmdMombo=(LinearLayout)findViewById(R.id.cmdMombo);
        cmdMombo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.dlgPersonajeAbierto=true;
                dlgEntrenador.mostrarDialog(Global.MOMBO);
            }
        });
        cmdPirolo=(LinearLayout)findViewById(R.id.cmdPirolo);
        cmdPirolo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.dlgPersonajeAbierto=true;
                dlgEntrenador.mostrarDialog(Global.PIROLO);
            }
        });
        cmdLucas=(LinearLayout)findViewById(R.id.cmdLucas);
        cmdLucas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.dlgPersonajeAbierto=true;
                dlgEntrenador.mostrarDialog(Global.LUCAS);
            }
        });
        new GeneraRedCreciente().execute();
    }

    @Override
    public void onBackPressed() {
        if(Global.dlgPersonajeAbierto){
            Global.dlgPersonajeAbierto=false;
            dlgEntrenador.ocultarDialog();
        }else{
            Global.numNodosValidos=0;
            Global.permisoParaEntregar=true;
            startActivity(new Intent(FrmMapa.this, FrmSplash.class));
            finish();
        }
    }

    public static int getResId(String resName, Class<?> c) {

        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    class GeneraRedCreciente extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... params) {
            grafo = new Grafo();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ProgressLayout.setVisibility(GONE);
            Toast.makeText(FrmMapa.this, "Se crearon "+Global.numNodosValidos+" nodos en la red creciente", Toast.LENGTH_LONG).show();
            if(Global.nodos[Global.yCasa][Global.xCasa]==null){
                Global.permisoParaEntregar=false;
                Toast.makeText(FrmMapa.this, "Es imposible llegar del punto inicial "+Global.yInicialEntrenamiento+", "+Global.xInicialEntrenamiento+" a la casa. Genera otro mapa", Toast.LENGTH_LONG).show();
            }
        }
    }
}
