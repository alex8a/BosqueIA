package com.example.alexx.bosqueia;

import java.util.ArrayList;

/**
 * Created by Alexx on 30/11/2016.
 */

public class Nodo {
    public ArrayList<Ruta> rutas;
    public int posX, posY;
    public Nodo(int y,int x){
        Global.numNodosValidos++;
        rutas=new ArrayList<Ruta>();
        posX=x;
        posY=y;
    }

    public void buscaPosiblesRutas(){
        if(posY>0){
            if(posX>0){
                if(Global.modeloMapa[posY-1][posX-1]!=Global.MURO){
                    if(Global.nodos[posY-1][posX-1]==null){
                        Global.nodos[posY-1][posX-1]=new Nodo(posY-1,posX-1);
                        Global.nodos[posY-1][posX-1].buscaPosiblesRutas();
                    }
                    crearRutaANodo(Global.nodos[posY-1][posX-1]);
                }
            }
            if(Global.modeloMapa[posY-1][posX]!=Global.MURO){
                if(Global.nodos[posY-1][posX]==null){
                    Global.nodos[posY-1][posX]=new Nodo(posY-1,posX);
                    Global.nodos[posY-1][posX].buscaPosiblesRutas();
                }
                crearRutaANodo(Global.nodos[posY-1][posX]);
            }
            if(posX<Global.numColumnas-1){
                if(Global.modeloMapa[posY-1][posX+1]!=Global.MURO){
                    if(Global.nodos[posY-1][posX+1]==null){
                        Global.nodos[posY-1][posX+1]=new Nodo(posY-1,posX+1);
                        Global.nodos[posY-1][posX+1].buscaPosiblesRutas();
                    }
                    crearRutaANodo(Global.nodos[posY-1][posX+1]);
                }
            }
        }
        if(posX<Global.numColumnas-1){
            if(Global.modeloMapa[posY][posX+1]!=Global.MURO){
                if(Global.nodos[posY][posX+1]==null){
                    Global.nodos[posY][posX+1]=new Nodo(posY,posX+1);
                    Global.nodos[posY][posX+1].buscaPosiblesRutas();
                }
                crearRutaANodo(Global.nodos[posY][posX+1]);
            }
        }
        if(posY<Global.numFilas-1){
            if(posX<Global.numColumnas-1){
                if(Global.modeloMapa[posY+1][posX+1]!=Global.MURO){
                    if(Global.nodos[posY+1][posX+1]==null){
                        Global.nodos[posY+1][posX+1]=new Nodo(posY+1,posX+1);
                        Global.nodos[posY+1][posX+1].buscaPosiblesRutas();
                    }
                    crearRutaANodo(Global.nodos[posY+1][posX+1]);
                }
            }
            if(Global.modeloMapa[posY+1][posX]!=Global.MURO){
                if(Global.nodos[posY+1][posX]==null){
                    Global.nodos[posY+1][posX]=new Nodo(posY+1,posX);
                    Global.nodos[posY+1][posX].buscaPosiblesRutas();
                }
                crearRutaANodo(Global.nodos[posY+1][posX]);
            }
            if(posX>0){
                if(Global.modeloMapa[posY+1][posX-1]!=Global.MURO){
                    if(Global.nodos[posY+1][posX-1]==null){
                        Global.nodos[posY+1][posX-1]=new Nodo(posY+1,posX-1);
                        Global.nodos[posY+1][posX-1].buscaPosiblesRutas();
                    }
                    crearRutaANodo(Global.nodos[posY+1][posX-1]);
                }
            }
        }
        if(posX>0){
            if(Global.modeloMapa[posY][posX-1]!=Global.MURO){
                if(Global.nodos[posY][posX-1]==null){
                    Global.nodos[posY][posX-1]=new Nodo(posY,posX-1);
                    Global.nodos[posY][posX-1].buscaPosiblesRutas();
                }
                crearRutaANodo(Global.nodos[posY][posX-1]);
            }
        }
    }

    public void crearRutaANodo(Nodo nodo){
        if(Global.modeloMapa[posY][posX]!=Global.CASA){
            switch (Global.modeloMapa[posY][posX]){
                case Global.MONTANA:
                    rutas.add(new Ruta(nodo, Global.EsfuerzoMomboMontana, Global.EsfuerzoPiroloMontana, Global.EsfuerzoLucasMontana));
                    break;
                case Global.AGUA:
                    rutas.add(new Ruta(nodo, Global.EsfuerzoMomboAgua, Global.EsfuerzoPiroloAgua, Global.EsfuerzoLucasAgua));
                    break;
                case Global.BARRANCO:
                    rutas.add(new Ruta(nodo, Global.EsfuerzoMomboBarranco, Global.EsfuerzoPiroloAgua, Global.EsfuerzoLucasAgua));
                    break;
                case Global.PLANO:
                    rutas.add(new Ruta(nodo, Global.EsfuerzoMomboPlano, Global.EsfuerzoPiroloAgua, Global.EsfuerzoLucasAgua));
                    break;
                default:
                    break;
            }
        }
    }
}
