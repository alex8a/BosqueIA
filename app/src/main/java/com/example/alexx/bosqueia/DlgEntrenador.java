package com.example.alexx.bosqueia;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;
import java.util.RandomAccess;
import java.util.concurrent.Exchanger;

/**
 * Created by Alexx on 01/12/2016.
 */

public class DlgEntrenador extends Fragment {
    View myView;
    Random random;
    ArrayList<Nodo> listaNodos;
    ArrayList<Ruta> listaRutas;
    RelativeLayout matrizMapa[][];
    RelativeLayout dlgPersonaje;
    Button cmdEntrenar, cmdSoltar;
    ImageView imgPersonaje;
    LinearLayout layContador;
    TextView txtContador;
    int xUsuario;
    int yUsuario;
    boolean permisoParaElegirCasilla;
    int[] maxEsfuerzo;
    int[] minEsfuerzo;
    int personaje;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView=inflater.inflate(R.layout.dlgentrenador,container,false);
        permisoParaElegirCasilla=false;
        random = new Random();
        layContador=(LinearLayout)myView.findViewById(R.id.layContador);
        txtContador=(TextView) myView.findViewById(R.id.txtContador);
        matrizMapa=new RelativeLayout[20][15];
        for(int i=0;i<20;i++){
            for(int j=0;j<15;j++){
                matrizMapa[i][j]=(RelativeLayout)myView.findViewById(getResources().getIdentifier("mm"+i+"_"+j, "id", getActivity().getApplicationContext().getPackageName()));
                //matrizMapa[i][j].setOnClickListener(this);
            }
        }
        matrizMapa[Global.yInicialEntrenamiento][Global.xInicialEntrenamiento].setBackgroundResource(R.mipmap.inicialicon);
        dlgPersonaje=(RelativeLayout)myView.findViewById(R.id.dlgPersonaje);
        imgPersonaje=(ImageView)myView.findViewById(R.id.imgPersonaje);
        cmdEntrenar=(Button)myView.findViewById(R.id.cmdEntrenar);
        cmdEntrenar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgPersonaje.setVisibility(View.GONE);
                new ComenzarEntrenamiento().execute();
            }
        });
        cmdSoltar=(Button)myView.findViewById(R.id.cmdSoltar);
        cmdSoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlgPersonaje.setVisibility(View.GONE);
                permisoParaElegirCasilla=true;
                new ImprimirMejorRuta().execute();
            }
        });
        maxEsfuerzo=new int[Global.NUMPERSONAJES];
        minEsfuerzo=new int[Global.NUMPERSONAJES];
        for(int i=0;i<Global.NUMPERSONAJES;i++){
            maxEsfuerzo[i]=0;
            minEsfuerzo[i]=0;
        }

        return myView;
    }

    public void mostrarDialog(int personajeP){
        personaje=personajeP;
        dlgPersonaje.setVisibility(View.VISIBLE);
        switch (personaje){
            case Global.MOMBO:
                imgPersonaje.setImageResource(R.drawable.imgmombo);
                break;
            case Global.PIROLO:
                imgPersonaje.setImageResource(R.drawable.imgpirolo);
                break;
            case Global.LUCAS:
                imgPersonaje.setImageResource(R.drawable.imglucas);
                break;
            default:
                break;
        }
    }

    public void ocultarDialog(){
        dlgPersonaje.setVisibility(View.GONE);
    }


    public void Entrena(int personaje){
        listaNodos=new ArrayList<Nodo>();
        listaRutas=new ArrayList<Ruta>();

    }

    public boolean nodoYaVisitado(Nodo nodo){
        for(int i=0;i<listaNodos.size();i++){
            if(nodo==listaNodos.get(i)) return true;
        }
        return false;
    }

    public int dameOcurrenciasDeNodo(Nodo nodo){
        int contador=0;
        for(int i=0;i<listaNodos.size();i++){
            if(nodo==listaNodos.get(i)) contador++;
        }
        return contador;
    }

    public int dameRutaMenosVisitada(Nodo nodo){
        int menoresOcurrencias=10;
        int nodoMenosVisitado=0;
        for(int i=0;i<nodo.rutas.size();i++){
            if(menoresOcurrencias>dameOcurrenciasDeNodo(nodo.rutas.get(i).nodoDestino)){
                nodoMenosVisitado=i;
                menoresOcurrencias=dameOcurrenciasDeNodo(nodo.rutas.get(i).nodoDestino);
            }
        }
        return nodoMenosVisitado;
    }

    public int dameSumatoriaEsfuerzo(){
        int sumatoria=0;
        for(int i=0;i<listaRutas.size();i++){
            sumatoria+=listaRutas.get(i).esfuerzos[personaje];
        }
        return sumatoria;
    }

    public float damePromedioMaxMin(){
        return (maxEsfuerzo[personaje]+minEsfuerzo[personaje])/2;
    }

    public void sumarAjusteARutas(float ajuste){
        for(int i=0;i<listaRutas.size();i++){
            listaRutas.get(i).esfuerzos[personaje]+=ajuste;
        }
    }

    class ComenzarEntrenamiento extends AsyncTask<Void, Void, Void>{
        Ruta rutaTemp;
        Nodo nodoActual, nodoAuxiliar;

        int xActual, yActual;
        int xPrevio, yPrevio;
        int contadorEntrenamientos;
        int sumatoriaEsfuerzo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            xActual=Global.xInicialEntrenamiento;
            xPrevio=Global.xInicialEntrenamiento;
            yActual=Global.yInicialEntrenamiento;
            yPrevio=Global.yInicialEntrenamiento;
            layContador.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            int indiceRutaRandom;
            int indiceAux;
            float ajuste;
            for(contadorEntrenamientos=0;contadorEntrenamientos<500;contadorEntrenamientos++){
                listaNodos=new ArrayList<Nodo>();
                listaRutas=new ArrayList<Ruta>();
                nodoActual=Global.nodos[Global.yInicialEntrenamiento][Global.xInicialEntrenamiento];
                listaNodos.add(nodoActual);
                yActual=Global.yInicialEntrenamiento;
                xActual=Global.xInicialEntrenamiento;
                publishProgress();
                while(nodoActual!=Global.nodos[Global.yCasa][Global.xCasa]){
                    try{
                        Thread.sleep(1);
                    } catch (Exception e){

                    }
                    indiceRutaRandom = random.nextInt(nodoActual.rutas.size());
                    indiceAux=indiceRutaRandom;
                    nodoAuxiliar=nodoActual.rutas.get(indiceAux).nodoDestino;
                    while(nodoYaVisitado(nodoAuxiliar)){
                        indiceAux++;
                        indiceAux%=nodoActual.rutas.size();
                        nodoAuxiliar=nodoActual.rutas.get(indiceAux).nodoDestino;
                        if(indiceAux==indiceRutaRandom) break;
                    }
                    rutaTemp=nodoActual.rutas.get(indiceAux);
                    listaRutas.add(listaRutas.size(),rutaTemp);
                    nodoActual=rutaTemp.nodoDestino;
                    listaNodos.add(nodoActual);
                    yActual=nodoActual.posY;
                    xActual=nodoActual.posX;
                    publishProgress();
                }
                sumatoriaEsfuerzo=dameSumatoriaEsfuerzo();
                if(minEsfuerzo[personaje]==0&&maxEsfuerzo[personaje]==0){
                    minEsfuerzo[personaje]=maxEsfuerzo[personaje]=sumatoriaEsfuerzo;
                }else{
                    damePromedioMaxMin();
                    ajuste=sumatoriaEsfuerzo-damePromedioMaxMin();
                    sumarAjusteARutas(ajuste);
                    if(maxEsfuerzo[personaje]<sumatoriaEsfuerzo){
                        maxEsfuerzo[personaje]=sumatoriaEsfuerzo;
                    }
                    if(minEsfuerzo[personaje]>sumatoriaEsfuerzo){
                        minEsfuerzo[personaje]=sumatoriaEsfuerzo;
                    }
                }

            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            switch (personaje){
                case Global.MOMBO:
                    matrizMapa[yActual][xActual].setBackgroundResource(R.drawable.imgmombo);
                    break;
                case Global.PIROLO:
                    matrizMapa[yActual][xActual].setBackgroundResource(R.drawable.imgpirolo);
                    break;
                case Global.LUCAS:
                    matrizMapa[yActual][xActual].setBackgroundResource(R.drawable.imglucas);
                    break;
                default:
                    break;
            }
            matrizMapa[yPrevio][xPrevio].setBackgroundResource(android.R.color.transparent);
            yPrevio=yActual;
            xPrevio=xActual;
            txtContador.setText(String.valueOf(contadorEntrenamientos));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            layContador.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "El entrenamiento de "+personaje+" ha terminado"+listaRutas.size()+listaNodos.size(),Toast.LENGTH_LONG).show();
            switch (personaje){
                case Global.MOMBO:
                    matrizMapa[Global.yCasa][Global.xCasa].setBackgroundResource(R.drawable.imgmombo);
                    break;
                case Global.PIROLO:
                    matrizMapa[Global.yCasa][Global.xCasa].setBackgroundResource(R.drawable.imgpirolo);
                    break;
                case Global.LUCAS:
                    matrizMapa[Global.yCasa][Global.xCasa].setBackgroundResource(R.drawable.imglucas);
                    break;
                default:
                    break;
            }
        }
    }

    class ImprimirMejorRuta extends AsyncTask<Void, Void, Void>{
        Ruta rutaTemp;
        Nodo nodoActual, nodoAuxiliar;

        int xActual, yActual;
        int xPrevio, yPrevio;
        int contadorEntrenamientos;
        int sumatoriaEsfuerzo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            xActual=Global.xInicialEntrenamiento;
            xPrevio=Global.xInicialEntrenamiento;
            yActual=Global.yInicialEntrenamiento;
            yPrevio=Global.yInicialEntrenamiento;
            layContador.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            int mejorIndice;
            int indiceAux;
            float mejorPeso;
            listaNodos=new ArrayList<Nodo>();
            listaRutas=new ArrayList<Ruta>();
            nodoActual=Global.nodos[Global.yInicialEntrenamiento][Global.xInicialEntrenamiento];
            while(nodoActual!=Global.nodos[Global.yCasa][Global.xCasa]){
                mejorIndice=0;
                mejorPeso=1000;
                for(int i =0;i<nodoActual.rutas.size();i++){
                    if(nodoActual.rutas.get(i).ponderaciones[personaje]<mejorPeso){
                        mejorIndice=i;
                        mejorPeso=nodoActual.rutas.get(i).ponderaciones[personaje];
                    }
                }
                nodoAuxiliar=nodoActual.rutas.get(mejorIndice).nodoDestino;
                indiceAux=mejorIndice;
                while(nodoYaVisitado(nodoAuxiliar)){
                    indiceAux++;
                    indiceAux%=nodoActual.rutas.size();
                    nodoAuxiliar=nodoActual.rutas.get(indiceAux).nodoDestino;
                    if(indiceAux==mejorIndice){
                        nodoAuxiliar=nodoActual.rutas.get(dameRutaMenosVisitada(nodoActual)).nodoDestino;
                        break;
                    }
                }
                nodoActual=nodoAuxiliar;
                listaNodos.add(nodoActual);
                yActual=nodoActual.posY;
                xActual=nodoActual.posX;
                publishProgress();
                try{
                    Thread.sleep(1000);
                } catch (Exception e){

                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            switch (personaje){
                case Global.MOMBO:
                    matrizMapa[yActual][xActual].setBackgroundResource(R.drawable.imgmombo);
                    break;
                case Global.PIROLO:
                    matrizMapa[yActual][xActual].setBackgroundResource(R.drawable.imgpirolo);
                    break;
                case Global.LUCAS:
                    matrizMapa[yActual][xActual].setBackgroundResource(R.drawable.imglucas);
                    break;
                default:
                    break;
            }
            matrizMapa[yPrevio][xPrevio].setBackgroundResource(android.R.color.transparent);
            yPrevio=yActual;
            xPrevio=xActual;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            layContador.setVisibility(View.GONE);
            switch (personaje){
                case Global.MOMBO:
                    matrizMapa[Global.yCasa][Global.xCasa].setBackgroundResource(R.drawable.imgmombo);
                    break;
                case Global.PIROLO:
                    matrizMapa[Global.yCasa][Global.xCasa].setBackgroundResource(R.drawable.imgpirolo);
                    break;
                case Global.LUCAS:
                    matrizMapa[Global.yCasa][Global.xCasa].setBackgroundResource(R.drawable.imglucas);
                    break;
                default:
                    break;
            }
        }
    }

}
